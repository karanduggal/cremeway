const express = require('express');
const router = express.Router();
const DiliverySchdulesController = require('../controller/deliverySchdules.controller');
const diliverySchdulesController = new DiliverySchdulesController();
// const userAuth = require('../middleware/auth')

/* GET users listing. */
router.route('/')
    .post(diliverySchdulesController.addDeliverySchdule)
    .get(diliverySchdulesController.getAllDeliverySchdules);

router.route('/:id')
    .delete(diliverySchdulesController.deleteDeliverySchdule)
    .get(diliverySchdulesController.getDeliverySchdule)
    .put(diliverySchdulesController.updateDeliverySchdule)

module.exports = router;
