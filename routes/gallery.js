const express = require('express');
const router = express.Router();
const GalleryController = require('../controller/gallery.controller');
const galleryController = new GalleryController();
// const userAuth = require('../middleware/auth')

/* GET users listing. */
router.route('/images')
    .post(galleryController.addGalleryImages)
    .get(galleryController.getAllGalleryImages);
router.route('/images/:id')
    .delete(galleryController.deleteGalleryImages)
    .get(galleryController.getGalleryImages)
    .put(galleryController.updateGalleryImages)
router.route('/')
    .post(galleryController.addGallery)
    .get(galleryController.getAllGallery);

router.route('/:id')
    .delete(galleryController.deleteGallery)
    .get(galleryController.getGallery)
    .put(galleryController.updateGallery)

module.exports = router;
