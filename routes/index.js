const express = require('express');
const router = express.Router();
const userRoutes = require('./users');
const productRoutes = require('./products');
const regionsRoutes = require('./regions');
const areasRoutes = require('./areas');
const deliverySchdulesRoutes = require('./deliverySchdules');
const routeRoutes = require('./route');
const unitsRoutes = require('./units');
const vehiclesRoutes = require('./vehicles');
const staticPageRoutes = require('./staticPage');
const galleryRoutes = require('./gallery');
const contactUsRoutes = require('./contactUs');
/* GET home page. */
router.use('/auth', userRoutes);
router.use('/product', productRoutes);
router.use('/regions', regionsRoutes);
router.use('/areas', areasRoutes);
router.use('/deliverySchdules', deliverySchdulesRoutes);
router.use('/routes', routeRoutes);
router.use('/units', unitsRoutes);
router.use('/vehicles', vehiclesRoutes);
router.use('/staticpage', staticPageRoutes);
router.use('/gallery', galleryRoutes);
router.use('/contactus', contactUsRoutes);

module.exports = router;
