const express = require('express');
const router = express.Router();
const RegionsController = require('../controller/regions.controller');
const regionsController = new RegionsController();
// const userAuth = require('../middleware/auth')

/* GET users listing. */
router.route('/')
    .post(regionsController.addRegions)
    .get(regionsController.getAllRegions);

router.route('/:id')
    .delete(regionsController.deleteRegion)
    .get(regionsController.getRegion)
    .put(regionsController.updateRegion)

module.exports = router;
