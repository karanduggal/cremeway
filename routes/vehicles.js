const express = require('express');
const router = express.Router();
const VehiclesController = require('../controller/vehicles.controller');
const vehiclesController = new VehiclesController();
// const userAuth = require('../middleware/auth')

/* GET users listing. */
router.route('/')
    .post(vehiclesController.addVehicle)
    .get(vehiclesController.getAllVehicles);

router.route('/:id')
    .delete(vehiclesController.deleteVehicle)
    .get(vehiclesController.getVehicle)
    .put(vehiclesController.updateVehicle)

module.exports = router;
