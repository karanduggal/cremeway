const express = require('express');
const router = express.Router();
const ContactUsController = require('../controller/contactUs.controller');
const contactUsController = new ContactUsController();
// const userAuth = require('../middleware/auth')

/* GET users listing. */
router.route('/')
    .post(contactUsController.addContactUs)
    .get(contactUsController.getAllContactUs);

router.route('/:id')
    .delete(contactUsController.deleteContactUs)
    .get(contactUsController.getContactUs)
    .put(contactUsController.updateContactUs)

module.exports = router;
