const express = require('express');
const router = express.Router();
const RouteController = require('../controller/route.controller');
const routeController = new RouteController();
// const userAuth = require('../middleware/auth')

/* GET users listing. */
router.route('/')
    .post(routeController.addRoute)
    .get(routeController.getAllRoute);

router.route('/:id')
    .delete(routeController.deleteRoute)
    .get(routeController.getRoute)
    .put(routeController.updateRoute)

module.exports = router;
