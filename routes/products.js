const express = require('express');
const router = express.Router();
const ProductController = require('../controller/product.controller');
const productController = new ProductController();
// const userAuth = require('../middleware/auth')

const multer = require('multer');

const storage = multer.memoryStorage();
const ImageUpload = multer({ storage: storage });

/* GET users listing. */
router.route('/category')
    .post(productController.addCategory)
    .get(productController.getAllCategory);

router.route('/category/:id')
    .get(productController.getCategory)
    .put(productController.updateCategory)
    .delete(productController.deleteCategory);

router.route('/')
    .post(ImageUpload.single('image'),productController.addProduct)
    .get(productController.getAllProduct);

router.route('/:id')
    .delete(productController.deleteProduct)
    .get(productController.getProduct)
    .put(ImageUpload.single('image'),productController.updateProduct)

module.exports = router;
