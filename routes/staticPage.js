const express = require('express');
const router = express.Router();
const StaticPageController = require('../controller/staticPage.controller');
const staticPageController = new StaticPageController();
// const userAuth = require('../middleware/auth')

/* GET users listing. */
router.route('/')
    .post(staticPageController.addStaticPage)
    .get(staticPageController.getAllStaticPage);

router.route('/:id')
    .delete(staticPageController.deleteStaticPage)
    .get(staticPageController.getStaticPage)
    .put(staticPageController.updateStaticPage)

module.exports = router;
