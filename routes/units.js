const express = require('express');
const router = express.Router();
const UnitsController = require('../controller/units.controller');
const unitsController = new UnitsController();
// const userAuth = require('../middleware/auth')

/* GET users listing. */
router.route('/')
    .post(unitsController.addUnit)
    .get(unitsController.getAllUnits);

router.route('/:id')
    .delete(unitsController.deleteUnit)
    .get(unitsController.getUnit)
    .put(unitsController.updateUnit)

module.exports = router;
