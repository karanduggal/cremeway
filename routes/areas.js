const express = require('express');
const router = express.Router();
const AreasController = require('../controller/areas.controller');
const areasController = new AreasController();
// const userAuth = require('../middleware/auth')

/* GET users listing. */
router.route('/')
    .post(areasController.addAreas)
    .get(areasController.getAllAreas);

router.route('/:id')
    .delete(areasController.deleteAreas)
    .get(areasController.getArea)
    .put(areasController.updateAreas)

module.exports = router;
