const express = require('express');
const router = express.Router();
const UserController = require('../controller/user.controller');
const userController = new UserController();
// const userAuth = require('../middleware/auth')

/* GET users listing. */
router.route('/signup')
    .post(userController.Signup);
router.route('/login')
    .post(userController.Login);
// router.route('/forgetpassword')
//     .post(userController.ForgetPassword);
// router.route('/updatepassword')
//     .post(userController.UpdatePassword);
router.route('/verify')
    .post(userController.Verifed_OTP);
// router.route('/updateprofile')
//     .post(userAuth,userController.UpdateProfile);
router.route('/users')
    .get(userController.getUsers);

module.exports = router;
