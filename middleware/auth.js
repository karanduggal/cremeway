// ========= services ===========
const DbService = require('../services/DB.services');
const dbService = new DbService();
// ========= NPM packages ===========
const jwt = require('jsonwebtoken');
// ========= constant file ===========
const constant = require('../config/constant');
// ========= model file ===========
const { UserModel } = require('../models')

module.exports = (...args) => async (req, res, next) => {
    try {
        if (!args.length) { throw "Invalid Role" }
        const token = req.headers.authorization.split(" ")[1]
        const decodedToken = jwt.verify(token, constant.JWT_SECRET);
        if (args[0] == 'isAdmin') {
            const User = await dbService.find(UserModel, { where: { email_id: decodedToken.email_id, type_user: decodedToken.type_user, } });
            req.user = User.dataValues;
            return next();
        }
        if (args[0] == 'isUser') {
            const User = await dbService.find(UserModel, { where: { phoneNo: decodedToken.phoneNo } });
            req.user = User.dataValues;
            return next();
        }
        throw "Auth Failed"
    } catch (error) {
        res.status(401).json({
            message: "Auth Failed! "
        })
    }
}