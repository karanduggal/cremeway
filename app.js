const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors')

// database
const db = require('./db/config')
// run()
// async function run() {
//     console.log("in run finction");
//     const data = await db.select('*').from('users')
//     console.log('data', data)
//     console.log('after db call');
// }

const indexRouter = require('./routes/index');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

app.use('/api', indexRouter);

module.exports = app;
