const Joi = require("joi");
const joiObjectid = require("joi-objectid");
Joi.objectId = require('joi-objectid')(Joi)

class ValidatorService {
    constructor() {
        this.schemas = {};
        this.initializeScemas();
    }
    initializeScemas() {
        // AUTH
        this.schemas.signupSchema = Joi.object({
            email_id: Joi.string().when(
                'device_type', {
                is: "admin",
                then: Joi.string().required()
            }
            ),
            phoneNo: Joi.number().when(
                'device_type', {
                is: "customer",
                then: Joi.number().required()
            }
            ),
            role_id: Joi.number().required(),
            type_user: Joi.string().required(),
            houseNo: Joi.string(),
            name: Joi.string(),
            device_type: Joi.string(),
            password: Joi.string(),
        });
        this.schemas.loginSchema = Joi.object({
            username: Joi.string().when(
                'type_user', {
                is: "admin",
                then: Joi.string().required()
            }),
            phoneNo: Joi.number().when(
                'type_user', {
                is: "customer",
                then: Joi.number().required()
            }),
            password: Joi.string().allow(null, ''),
            type_user: Joi.string(),
            device_type: Joi.string(),
            role_id: Joi.number(),
            name: Joi.string().allow(null, ''),
            houseNo: Joi.string().allow(null, ''),
        });
        this.schemas.forgetpasswordSchema = Joi.object({
            type: Joi.string().required(),
            deviceType: Joi.string(),
            email: Joi.string().when(
                'type', {
                is: "admin",
                then: Joi.string().required()
            }
            ),
            phoneNumber: Joi.number().when(
                'type', {
                is: "user",
                then: Joi.number().required()
            }
            ),
        });
        this.schemas.verifyotpSchema = Joi.object({
            type_user: Joi.string().required(),
            device_type: Joi.string(),
            token: Joi.string().required(),
            otp: Joi.string().required(),
            phoneNo: Joi.string().required(),
        });
        this.schemas.updatepasswordSchema = Joi.object({
            type: Joi.string().required(),
            deviceType: Joi.string(),
            token: Joi.string().required(),
            password: Joi.string().required(),
        });
        this.schemas.productSchema = Joi.object({
            type: Joi.string().required(),
            deviceType: Joi.string(),
            productName: Joi.string().required(),
            productType: Joi.string().required(),
            productCategory: Joi.string().required(),
            description: Joi.string().required(),
            price: Joi.number().required(),
        });
        this.schemas.updateProfileSchema = Joi.object({
            type_user: Joi.string().required(),
            name: Joi.string(),
            email_id: Joi.string(),
            device_id: Joi.string(),
            region_id: Joi.number(),
            area_id: Joi.number(),
            latitude: Joi.string(),
            longitude: Joi.string(),
            houseNo: Joi.string(),
            username: Joi.string(),
            password: Joi.string(),
            group_id: Joi.number(),
            role_id: Joi.number(),
            customer_category: Joi.string(),
            image: Joi.string(),
            token: Joi.string(),
            status: Joi.number(),
            is_deleted: Joi.boolean(),
            language: Joi.string(),
            assignee: Joi.string(),
            message: Joi.string()
        });
        // category
        this.schemas.category = Joi.object({
            deviceType: Joi.string(),
            name: Joi.string().required(),
            category_image: Joi.string().required(),
            status: Joi.number().required(),
            is_deleteable: Joi.number().required(),
        });
        // PRoducts
        this.schemas.product = Joi.object({
            product: Joi.object({
                category_id: Joi.number().required(),
                name: Joi.string().required(),
                is_subscribable: Joi.number().required(),
                price_per_unit: Joi.number().required(),
                quantity: Joi.number().required(),
                in_stock: Joi.number().required(),
                iscontainer: Joi.number().required(),
                unit_id: Joi.number().required(),
                status: Joi.number(),
                image: Joi.string().required(),
                description: Joi.string().required(),
                position: Joi.number().required(),
            }).required().label("Product Object"),
            product_child: Joi.array().items({
                id: Joi.number().allow(null, ''),
                quantity: Joi.number().required().label("product_child.quantity"),
                in_stock: Joi.number().required().label("product_child.in_stock"),
                unit_id: Joi.number().required().label("product_child.unit_id"),
                price: Joi.number().required().label("product_child.price"),
            }),
            areas: Joi.array().items(Joi.number())
        });
        // Resions
        this.schemas.regions = Joi.object({
            name: Joi.string().required(),
            status: Joi.number().allow(null, ''),
            enable_sms: Joi.number().allow(null, ''),
        });
        // Areas
        this.schemas.areas = Joi.object({
            name: Joi.string().required(),
            status: Joi.number().allow(null, ''),
            region_id: Joi.number().required(),
            manager_name: Joi.string().required(),
            manager_phoneNo: Joi.number().required(),
            manager_email: Joi.string().required(),
        });
        // delivery Schdules
        this.schemas.diliverySchdule = Joi.object({
            name: Joi.string().required(),
            status: Joi.number().required(),
            start_time: Joi.string().required(),
            end_time: Joi.string().required(),
            edit_time: Joi.number().required(),
        });
        // Routes
        this.schemas.routes = Joi.object({
            name: Joi.string().required(),
        });
        // Units
        this.schemas.units = Joi.object({
            name: Joi.string().required(),
            status: Joi.number().required(),
        });
        // vehicles
        this.schemas.vehicles = Joi.object({
            vehicle_no: Joi.string().required(),
            status: Joi.number().required(),
        });
        // Static page
        this.schemas.staticPage = Joi.object({
            title: Joi.string().required(),
            description: Joi.string().required(),
            page: Joi.string().required(),
            address: Joi.string().required(),
        });
        // Gallery
        this.schemas.gallery = Joi.object({
            title: Joi.string().required(),
            description: Joi.string().required(),
            directory: Joi.string().required(),
        });
        // Gallery
        this.schemas.galleryImages = Joi.object({
            gallery_id: Joi.number().required(),
            thumbnail: Joi.string().required(),
            image: Joi.string().required(),
            position: Joi.number().required(),
        });
        // Contact Us
        this.schemas.conractUs = Joi.object({
            address: Joi.string().required(),
            name: Joi.string().required(),
            phoneNo: Joi.number().required(),
            email_id: Joi.string().required(),
        });
    }
}
module.exports = ValidatorService
// id, address, name, phoneNo, email_id
