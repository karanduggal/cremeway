"use strict";
const constant = require('../config/constant')
const nodemailer = require("nodemailer");

class MailService {
    constructor() { }
    send(token,email) {
        return new Promise(async (resolve, reject) => {
            try {
                let transporter = nodemailer.createTransport({
                    host: "smtp.gmail.com",
                    auth: {
                        user: constant.EmialId,
                        pass: constant.EmialPassword,
                    },
                });
                let info = await transporter.sendMail({
                    from: constant.EmialId,
                    to: email,
                    subject: "Hello ✔",
                    text: "Hello world?",
                    html: `<h1>Email</h1>${token}<br><br><h1>Token</h1>${email}`,
                });
                resolve(true)
            } catch (err) {
                reject(err);
            }
        })
    }
}
module.exports = MailService
