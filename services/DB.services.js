class DBService {
    constructor() { }
    /**
     * Finds single object in database.
     * @param {*} TableName Name of table.
     * @param {*} findQuery Query to be executed.
     * @returns object
     */
    find(TableName,findQuery) {
        return new Promise(async (resolve, reject) => {
            try {
                resolve(await TableName.findOne(findQuery));
            } catch (err) {
                reject(err);
            }
        })
    }
    create(TableName,body) {
        return new Promise(async (resolve, reject) => {
            try {
                resolve(await TableName.create(body));
            } catch (err) {
                reject(err);
            }
        })
    }
    createByForeignKey(TableName,foreignKeyObj,body) {
        return new Promise(async (resolve, reject) => {
            try {
                resolve(await foreignKeyObj.createProduct(body));
            } catch (err) {
                reject(err);
            }
        })
    }
    update(TableName,updateQuery,findQuery){
        return new Promise(async (resolve, reject) => {
            try {                
                resolve(await TableName.update(updateQuery, findQuery));
            } catch (err) {
                reject(err);
            }
        })
    }
}
module.exports = DBService