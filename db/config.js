const knex = require('knex')
const constant = require('./constant')
const DB = knex({
    client: 'mysql2',
    connection: {
        host: constant.DB_HOST,
        user: constant.DB_USER,
        password: constant.DB_PASSWORD,
        database: constant.DB_DATABASE_NAME,  
    }
});

module.exports = DB