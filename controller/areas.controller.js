// ------------------ Packages -------------------------
const moment = require('moment');
// ------------------ Services -------------------------
const ValidatorService = require('../services/validator.services');
const validatorService = new ValidatorService();
// ------------------ Model -------------------------
const constant = require('../db/constant');
// ------------------ dataBase -------------------------
const db = require('../db/config')

module.exports = AreasController = function () {
    this.addAreas = async (req, res) => {
        try {
            const validate = await validatorService.schemas.areas.validate(req.body)
            if (validate.error) { throw validate.error.details[0].message }
            validate.value.created = moment().format('YYYY-MM-DD h:mm:ss');
            validate.value.modified = moment().format('YYYY-MM-DD h:mm:ss');
            const region = await db.select('*').from('regions').where({ id: validate.value.region_id });
            if (!region[0]) { throw "Invalid Region" }
            const areas = await db('areas').insert(validate.value)
            return res.status(200).json({ success: true, message: constant.AREAS_ADD, data: areas });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.getAllAreas = async (req, res) => {
        try {
            // req.query.search = req.query ? req.query.search ? req.query.search : '' : '';
            req.query.pageNo = req.query ? req.query.pageNo ? req.query.pageNo : 0 : 0;
            // req.query.limit = req.query ? req.query.limit ? req.query.limit : "" : "";
            const count = await db.raw(`select count(*) as count FROM areas`)
            if (req.query.limit) {
                if (req.query.pageNo != 0) { req.query.pageNo = (req.query.pageNo - 1) * 10 }
                req.query.limit = req.query ? req.query.limit ? req.query.limit : 10 : 10;
                const first = `SELECT
                areas.id,areas.name,areas.status,areas.region_id,areas.created,areas.modified,areas.manager_name,areas.manager_phoneNo,areas.manager_email,
                regions.name as regionName,regions.status as regionStatus,regions.enable_sms as regionEnable_sms,regions.created as regionCreated,regions.modified as regionModified
                FROM areas 
                LEFT JOIN regions ON areas.region_id=regions.id`;
                const second = `WHERE areas.name LIKE '%${req.query.search}%'`;
                const third = `limit ${req.query.pageNo},${req.query.limit};`
                // const areas = await db.raw(`SELECT
                // areas.id,areas.name,areas.status,areas.region_id,areas.created,areas.modified,areas.manager_name,areas.manager_phoneNo,areas.manager_email,
                // regions.name as regionName,regions.status as regionStatus,regions.enable_sms as regionEnable_sms,regions.created as regionCreated,regions.modified as regionModified
                // FROM areas 
                // LEFT JOIN regions ON areas.region_id=regions.id
                // WHERE name LIKE '%${req.query.search}%' 
                // limit ${req.query.pageNo},${req.query.limit};`)
                if(req.query.search){
                    console.log(second);
                    const areas = await db.raw(`${first} ${second} ${third}`)
                    return res.status(200).json({ success: true, message: constant.AREAS_GET_ALL, data: areas[0],totalCount:count[0][0].count });
                }
                const areas = await db.raw(`${first} ${third}`)
                return res.status(200).json({ success: true, message: constant.AREAS_GET_ALL, data: areas[0],totalCount:count[0][0].count });
            }
            const areas = await db.raw(`SELECT
            areas.id,areas.name,areas.status,areas.region_id,areas.created,areas.modified,areas.manager_name,areas.manager_phoneNo,areas.manager_email,
            regions.name as regionName,regions.status as regionStatus,regions.enable_sms as regionEnable_sms,regions.created as regionCreated,regions.modified as regionModified
            FROM areas 
            LEFT JOIN regions ON areas.region_id=regions.id;`)
            return res.status(200).json({ success: true, message: constant.AREAS_GET_ALL, data: areas[0],totalCount:count[0][0].count });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.getArea = async (req, res) => {
        try {
            const areas = await db.select('*').from('areas').where({ id: req.params.id });
            return res.status(200).json({ success: true, message: constant.AREAS_GET, data: areas[0] });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.updateAreas = async (req, res) => {
        try {
            const validate = await validatorService.schemas.areas.validate(req.body)
            if (validate.error) { throw validate.error.details[0].message }
            validate.value.modified = moment().format('YYYY-MM-DD h:mm:ss');
            const region = await db.select('*').from('regions').where({ id: validate.value.region_id });
            if (!region[0]) { throw "Invalid Region" }
            const areas = await db('areas').where({ id: req.params.id }).update(validate.value);
            return res.status(200).json({ success: true, message: constant.AREAS_EDIT, data: areas });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.deleteAreas = async (req, res) => {
        try {
            const areas = await db('areas').where({ id: req.params.id }).del();
            return res.status(200).json({ success: true, message: constant.AREAS_DELETE, data: areas });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
}