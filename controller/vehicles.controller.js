// ------------------ Packages -------------------------
const moment = require('moment');
// ------------------ Services -------------------------
const ValidatorService = require('../services/validator.services');
const validatorService = new ValidatorService();
// ------------------ Model -------------------------
const constant = require('../db/constant');
// ------------------ dataBase -------------------------
const db = require('../db/config')

module.exports = VehicleController = function () {
    this.addVehicle = async (req, res) => {
        try {
            const validate = await validatorService.schemas.vehicles.validate(req.body)
            if (validate.error) { throw validate.error.details[0].message }
            const vehicles = await db('vehicles').insert(validate.value)
            return res.status(200).json({ success: true, message: constant.VEHICALS_ADD, data: vehicles });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.getAllVehicles = async (req, res) => {
        try {
            req.query.search = req.query ? req.query.search ? req.query.search : '' : '';
            req.query.pageNo = req.query ? req.query.pageNo ? req.query.pageNo : 0 : 0;
            req.query.limit = req.query ? req.query.limit ? req.query.limit : 10 : 10;
            if (req.query.pageNo != 0) { req.query.pageNo = (req.query.pageNo-1) *10}            
            const vehicles = await db.raw(`SELECT * FROM vehicles WHERE vehicle_no LIKE '%${req.query.search}%' limit ${req.query.pageNo},${req.query.limit};`);
            // const vehicles = await db.select('*').from('vehicles')
            return res.status(200).json({ success: true, message: constant.VEHICALS_GET_ALL, data: vehicles[0] });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.getVehicle = async (req, res) => {
        try {
            const vehicles = await db.select('*').from('vehicles').where({ id: req.params.id });
            return res.status(200).json({ success: true, message: constant.VEHICALS_GET, data: vehicles[0] });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.updateVehicle = async (req, res) => {
        try {
            const validate = await validatorService.schemas.vehicles.validate(req.body)
            if (validate.error) { throw validate.error.details[0].message }
            const vehicles = await db('vehicles').where({ id: req.params.id }).update(validate.value);
            return res.status(200).json({ success: true, message: constant.VEHICALS_EDIT, data: vehicles });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.deleteVehicle = async (req, res) => {
        try {
            const vehicles = await db('vehicles').where({ id: req.params.id }).del();
            return res.status(200).json({ success: true, message: constant.VEHICALS_DELETE, data: vehicles });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
}