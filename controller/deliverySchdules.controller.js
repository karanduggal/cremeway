// ------------------ Packages -------------------------
const moment = require('moment');
// ------------------ Services -------------------------
const ValidatorService = require('../services/validator.services');
const validatorService = new ValidatorService();
// ------------------ Model -------------------------
const constant = require('../db/constant');
// ------------------ dataBase -------------------------
const db = require('../db/config')

module.exports = DeliverySchduleController = function () {
    this.addDeliverySchdule = async (req, res) => {
        try {
            const validate = await validatorService.schemas.diliverySchdule.validate(req.body)
            if (validate.error) { throw validate.error.details[0].message }
            const delivery_schdules = await db('delivery_schdules').insert(validate.value)
            return res.status(200).json({ success: true, message: constant.DILIVERYSCHDULER_ADD, data: delivery_schdules });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.getAllDeliverySchdules = async (req, res) => {
        try {
            req.query.search = req.query ? req.query.search ? req.query.search : '' : '';
            req.query.pageNo = req.query ? req.query.pageNo ? req.query.pageNo : 0 : 0;
            req.query.limit = req.query ? req.query.limit ? req.query.limit : 10 : 10;
            if (req.query.pageNo != 0) { req.query.pageNo = (req.query.pageNo-1) *10}            
            const delivery_schdules = await db.raw(`SELECT * FROM delivery_schdules WHERE name LIKE '%${req.query.search}%' limit ${req.query.pageNo},${req.query.limit};`)
            // const delivery_schdules = await db.select('*').from('delivery_schdules')
            return res.status(200).json({ success: true, message: constant.DILIVERYSCHDULER_GET_ALL, data: delivery_schdules[0] });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.getDeliverySchdule = async (req, res) => {
        try {
            const delivery_schdules = await db.select('*').from('delivery_schdules').where({ id: req.params.id });
            return res.status(200).json({ success: true, message: constant.DILIVERYSCHDULER_GET, data: delivery_schdules[0] });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.updateDeliverySchdule = async (req, res) => {
        try {
            const validate = await validatorService.schemas.diliverySchdule.validate(req.body)
            if (validate.error) { throw validate.error.details[0].message }
            const delivery_schdules = await db('delivery_schdules').where({ id: req.params.id }).update(validate.value);
            return res.status(200).json({ success: true, message: constant.DILIVERYSCHDULER_EDIT, data: delivery_schdules });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.deleteDeliverySchdule = async (req, res) => {
        try {
            const delivery_schdules = await db('delivery_schdules').where({ id: req.params.id }).del();
            return res.status(200).json({ success: true, message: constant.DILIVERYSCHDULER_DELETE, data: delivery_schdules });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
}