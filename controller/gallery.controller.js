// ------------------ Packages -------------------------
const moment = require('moment');
// ------------------ Services -------------------------
const ValidatorService = require('../services/validator.services');
const validatorService = new ValidatorService();
// ------------------ Model -------------------------
const constant = require('../db/constant');
// ------------------ dataBase -------------------------
const db = require('../db/config')

module.exports = GalleryController = function () {
    this.addGallery = async (req, res) => {
        try {
            const validate = await validatorService.schemas.gallery.validate(req.body)
            if (validate.error) { throw validate.error.details[0].message }
            const gallery = await db('gallery').insert(validate.value)
            return res.status(200).json({ success: true, message: constant.GALLERY_ADD, data: gallery });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.getAllGallery = async (req, res) => {
        try {
            req.query.search = req.query ? req.query.search ? req.query.search : '' : '';
            req.query.pageNo = req.query ? req.query.pageNo ? req.query.pageNo : 0 : 0;
            req.query.limit = req.query ? req.query.limit ? req.query.limit : 10 : 10;
            if (req.query.pageNo != 0) { req.query.pageNo = (req.query.pageNo-1) *10}            
            const gallery = await db.raw(`SELECT * FROM gallery WHERE title LIKE '%${req.query.search}%' limit ${req.query.pageNo},${req.query.limit};`)
            // const gallery = await db.select('*').from('gallery')
            return res.status(200).json({ success: true, message: constant.GALLERY_GET_ALL, data: gallery[0] });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.getGallery = async (req, res) => {
        try {
            const gallery = await db.select('*').from('gallery').where({ id: req.params.id });
            return res.status(200).json({ success: true, message: constant.GALLERY_GET, data: gallery[0] });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.updateGallery = async (req, res) => {
        try {
            const validate = await validatorService.schemas.gallery.validate(req.body)
            if (validate.error) { throw validate.error.details[0].message }
            const gallery = await db('gallery').where({ id: req.params.id }).update(validate.value);
            return res.status(200).json({ success: true, message: constant.GALLERY_EDIT, data: gallery });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.deleteGallery = async (req, res) => {
        try {
            const gallery = await db('gallery').where({ id: req.params.id }).del();
            return res.status(200).json({ success: true, message: constant.GALLERY_DELETE, data: gallery });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }

    this.addGalleryImages = async (req, res) => {
        try {
            const validate = await validatorService.schemas.galleryImages.validate(req.body)
            if (validate.error) { throw validate.error.details[0].message }
            const galleryIdCheck = await db.select('*').from('gallery').where({ id: validate.value.gallery_id });
            if(!galleryIdCheck[0]){ throw constant.GALLERY_NOT_EXIST }
            const galleryImages = await db('gallery_images').insert(validate.value)
            return res.status(200).json({ success: true, message: constant.GALLERY_ADD, data: galleryImages });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.getAllGalleryImages = async (req, res) => {
        try {
            // req.query.search = req.query ? req.query.search ? req.query.search : '' : '';
            // req.query.pageNo = req.query ? req.query.pageNo ? req.query.pageNo : 0 : 0;
            // req.query.limit = req.query ? req.query.limit ? req.query.limit : 10 : 10;
            // if (req.query.pageNo != 0) { req.query.pageNo = (req.query.pageNo-1) *10}            
            // const galleryImages = await db.raw(`SELECT * FROM gallery_images WHERE title LIKE '%${req.query.search}%' limit ${req.query.pageNo},${req.query.limit};`)
            const galleryImages = await db.select('*').from('gallery_images')
            return res.status(200).json({ success: true, message: constant.GALLERY_GET_ALL, data: galleryImages });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.getGalleryImages = async (req, res) => {
        try {
            const galleryImages = await db.select('*').from('gallery_images').where({ id: req.params.id });
            return res.status(200).json({ success: true, message: constant.GALLERY_GET, data: galleryImages[0] });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.updateGalleryImages = async (req, res) => {
        try {
            const validate = await validatorService.schemas.galleryImages.validate(req.body)
            if (validate.error) { throw validate.error.details[0].message }
            const galleryIdCheck = await db.select('*').from('gallery').where({ id: validate.value.gallery_id });
            if(!galleryIdCheck[0]){ throw constant.GALLERY_NOT_EXIST }
            const galleryImages = await db('gallery_images').where({ id: req.params.id }).update(validate.value);
            return res.status(200).json({ success: true, message: constant.GALLERY_EDIT, data: galleryImages });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.deleteGalleryImages = async (req, res) => {
        try {
            const galleryImages = await db('gallery_images').where({ id: req.params.id }).del();
            return res.status(200).json({ success: true, message: constant.GALLERY_DELETE, data: galleryImages });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
}