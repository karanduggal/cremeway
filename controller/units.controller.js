// ------------------ Packages -------------------------
const moment = require('moment');
// ------------------ Services -------------------------
const ValidatorService = require('../services/validator.services');
const validatorService = new ValidatorService();
// ------------------ Model -------------------------
const constant = require('../db/constant');
// ------------------ dataBase -------------------------
const db = require('../db/config')

module.exports = UnitsController = function () {
    this.addUnit = async (req, res) => {
        try {
            const validate = await validatorService.schemas.units.validate(req.body)
            if (validate.error) { throw validate.error.details[0].message }
            validate.value.created = moment().format('YYYY-MM-DD h:mm:ss');
            validate.value.modified = moment().format('YYYY-MM-DD h:mm:ss');
            const units = await db('units').insert(validate.value)
            return res.status(200).json({ success: true, message: constant.UNIT_ADD, data: units });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.getAllUnits = async (req, res) => {
        try {
            req.query.search = req.query ? req.query.search ? req.query.search : '' : '';
            req.query.pageNo = req.query ? req.query.pageNo ? req.query.pageNo : 0 : 0;
            req.query.limit = req.query ? req.query.limit ? req.query.limit : 10 : 10;
            if (req.query.pageNo != 0) { req.query.pageNo = (req.query.pageNo-1) *10}            
            const units = await db.raw(`SELECT * FROM units WHERE name LIKE '%${req.query.search}%' limit ${req.query.pageNo},${req.query.limit};`);
            // const units = await db.select('*').from('units')
            return res.status(200).json({ success: true, message: constant.UNIT_GET_ALL, data: units[0] });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.getUnit = async (req, res) => {
        try {
            const units = await db.select('*').from('units').where({ id: req.params.id });
            return res.status(200).json({ success: true, message: constant.UNIT_GET, data: units[0] });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.updateUnit = async (req, res) => {
        try {
            const validate = await validatorService.schemas.units.validate(req.body)
            if (validate.error) { throw validate.error.details[0].message }
            validate.value.modified = moment().format('YYYY-MM-DD h:mm:ss');
            const units = await db('units').where({ id: req.params.id }).update(validate.value);
            return res.status(200).json({ success: true, message: constant.UNIT_EDIT, data: units });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.deleteUnit = async (req, res) => {
        try {
            const units = await db('units').where({ id: req.params.id }).del();
            return res.status(200).json({ success: true, message: constant.UNIT_DELETE, data: units });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
}