// ------------------ Packages -------------------------
const moment = require('moment');
// ------------------ Services -------------------------
const ValidatorService = require('../services/validator.services');
const validatorService = new ValidatorService();
// ------------------ Model -------------------------
const constant = require('../db/constant');
// ------------------ dataBase -------------------------
const db = require('../db/config')

module.exports = ContactUsController = function () {
    this.addContactUs = async (req, res) => {
        try {
            const validate = await validatorService.schemas.conractUs.validate(req.body)
            if (validate.error) { throw validate.error.details[0].message }
            const conractUs = await db('contacts').insert(validate.value)
            return res.status(200).json({ success: true, message: constant.CONTACTUS_ADD, data: conractUs });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.getAllContactUs = async (req, res) => {
        try {
            req.query.search = req.query ? req.query.search ? req.query.search : '' : '';
            req.query.pageNo = req.query ? req.query.pageNo ? req.query.pageNo : 0 : 0;
            req.query.limit = req.query ? req.query.limit ? req.query.limit : 10 : 10;
            if (req.query.pageNo != 0) { req.query.pageNo = (req.query.pageNo-1) *10}            
            const conractUs = await db.raw(`SELECT * FROM contacts WHERE name LIKE '%${req.query.search}%' limit ${req.query.pageNo},${req.query.limit};`)
            // const conractUs = await db.select('*').from('conractUs')
            return res.status(200).json({ success: true, message: constant.CONTACTUS_GET_ALL, data: conractUs[0] });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.getContactUs = async (req, res) => {
        try {
            const conractUs = await db.select('*').from('contacts').where({ id: req.params.id });
            return res.status(200).json({ success: true, message: constant.CONTACTUS_GET, data: conractUs[0] });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.updateContactUs = async (req, res) => {
        try {
            const validate = await validatorService.schemas.conractUs.validate(req.body)
            if (validate.error) { throw validate.error.details[0].message }
            const conractUs = await db('contacts').where({ id: req.params.id }).update(validate.value);
            return res.status(200).json({ success: true, message: constant.CONTACTUS_EDIT, data: conractUs });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.deleteContactUs = async (req, res) => {
        try {
            const conractUs = await db('contacts').where({ id: req.params.id }).del();
            return res.status(200).json({ success: true, message: constant.CONTACTUS_DELETE, data: conractUs });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
}