// ------------------ Packages -------------------------
const moment = require('moment');
// ------------------ Services -------------------------
const ValidatorService = require('../services/validator.services');
const validatorService = new ValidatorService();
// ------------------ Model -------------------------
const constant = require('../db/constant');
// ------------------ dataBase -------------------------
const db = require('../db/config')

module.exports = RouteController = function () {
    this.addRoute = async (req, res) => {
        try {
            const validate = await validatorService.schemas.routes.validate(req.body)
            if (validate.error) { throw validate.error.details[0].message }
            validate.value.created = moment().format('YYYY-MM-DD h:mm:ss');
            const routes = await db('routes').insert(validate.value)
            return res.status(200).json({ success: true, message: constant.ROUTE_ADD, data: routes });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.getAllRoute = async (req, res) => {
        try {
            req.query.search = req.query ? req.query.search ? req.query.search : '' : '';
            req.query.pageNo = req.query ? req.query.pageNo ? req.query.pageNo : 0 : 0;
            req.query.limit = req.query ? req.query.limit ? req.query.limit : 10 : 10;
            if (req.query.pageNo != 0) { req.query.pageNo = (req.query.pageNo-1) *10}            
            const routes = await db.raw(`SELECT * FROM routes WHERE name LIKE '%${req.query.search}%' limit ${req.query.pageNo},${req.query.limit};`)
            // const routes = await db.select('*').from('routes')
            return res.status(200).json({ success: true, message: constant.ROUTE_GET_ALL, data: routes[0] });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.getRoute = async (req, res) => {
        try {
            const routes = await db.select('*').from('routes').where({ id: req.params.id });
            return res.status(200).json({ success: true, message: constant.ROUTE_GET, data: routes[0] });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.updateRoute = async (req, res) => {
        try {
            const validate = await validatorService.schemas.routes.validate(req.body)
            if (validate.error) { throw validate.error.details[0].message }
            const routes = await db('routes').where({ id: req.params.id }).update(validate.value);
            return res.status(200).json({ success: true, message: constant.ROUTE_EDIT, data: routes });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.deleteRoute = async (req, res) => {
        try {
            const routes = await db('routes').where({ id: req.params.id }).del();
            return res.status(200).json({ success: true, message: constant.ROUTE_DELETE, data: routes });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
}