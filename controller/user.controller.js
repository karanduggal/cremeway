// ------------------ Packages -------------------------
// const bcrypt = require('bcrypt');
// const jwt = require('jsonwebtoken');
const moment = require('moment');
const md5 = require('md5')
// ------------------ Services -------------------------
const TokenService = require('../services/token.services');
const tokenService = new TokenService();
// const MailService = require('../services/mail.services');
// const mailService = new MailService();
// const DBService = require('../services/DB.services');
// const dbService = new DBService();
const ValidatorService = require('../services/validator.services');
const validatorService = new ValidatorService();
// const SmsService = require('../services/sms.services');
// const smsService = new SmsService();
// ------------------ Model -------------------------
// const {UserModel} = require('../models')
// const UserModel = require('../models').users;
const constant = require('../db/constant');
// ------------------ dataBase -------------------------
const db = require('../db/config')

module.exports = UserController = function () {
    this.Signup = async (req, res) => {
        try {
            const validate = await validatorService.schemas.signupSchema.validate(req.body)
            if (validate.error) { throw validate.error.details[0].message }
            validate.value.created = moment().format('YYYY-MM-DD h:mm:ss');
            validate.value.modified = moment().format('YYYY-MM-DD h:mm:ss');
            if (validate.value.device_type === "cms") {
                delete validate.value.device_type;
                const exist = await db.select('*').from('users').where({ email_id: validate.value.email_id, role_id: validate.value.role_id, type_user: validate.value.type_user });
                if (exist[0]) { throw constant.USER_EXIST }
                const user = await db('users').insert(validate.value)
                return res.status(200).json({ success: true, message: constant.USER_SIGNUP_SUCCESSFULLY, data: user/* , token: await tokenService.create(user, { }) */ });
            }
            if (validate.value.device_type === "web") {
                delete validate.value.device_type;
                validate.value.verifed_otp = false
                const exist = await db.select('*').from('users').where({ email_id: validate.value.email_id, role_id: validate.value.role_id, type_user: validate.value.type_user });
                if (exist[0]) { throw constant.USER_EXIST }
                // const user = await db('users').insert(validate.value)
                // const token  = await tokenService.create({email_id:validate.value.email_id,role_id:validate.value.role_id,type_user:validate.value.type_user},{});
                return res.status(200).json({ success: true, message: constant.SIGNUP_SUCCESSFULLY/* , data: user */, token: await tokenService.create({ email_id: validate.value.email_id, role_id: validate.value.role_id, type_user: validate.value.type_user }, {}) });
            }
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.Login = async (req, res) => {
        try {
            const validate = await validatorService.schemas.loginSchema.validate(req.body)
            if (validate.error) { throw validate.error.details[0].message }
            validate.value.created = moment().format('YYYY-MM-DD h:mm:ss'); validate.value.modified = moment().format('YYYY-MM-DD h:mm:ss');
            validate.value.otp = Math.floor(Math.random() * 10000);
            if (validate.value.phoneNo && validate.value.type_user === "customer" && validate.value.device_type ==="phone") {
                delete validate.value.device_type;
                const user = await db.select('*').from('users').where({ phoneNo: validate.value.phoneNo });
                if (user[0]) {
                    const update = await db('users').where({ phoneNo: validate.value.phoneNo }).update({ otp: validate.value.otp, modified: validate.value.modified });
                    if (update) { return res.status(200).json({ success: true, message: constant.LOGIN_SUCCESS, update: update, user: user, OTP: validate.value.otp, token: await tokenService.create({phoneNo:validate.value.phoneNo,type_user:validate.value.type_user}, { }) }); }
                    else { throw constant.NOT_UPDATE; }
                }
                if (!user[0]) {
                    const user = await db('users').insert(validate.value)
                    return res.status(200).json({ success: true, message: constant.SIGNUP_SUCCESSFULLY, data: user, OTP: validate.value.otp, token: await tokenService.create({phoneNo:validate.value.phoneNo,type_user:validate.value.type_user}, { }) });
                }
            }
            if (validate.value.username && validate.value.type_user === "admin" && validate.value.device_type ==="cms") {
                delete validate.value.device_type;
                const user = await db.select('*').from('users').where({ username: validate.value.username });
                if(!user[0]){ throw constant.USER_NOT_EXIST}
                if(md5(validate.value.password) != user[0].password){throw constant.USER_WRONG_PASSWORD}
                delete user[0].password
                return res.status(200).json({ success: true, message: constant.USER_LOGIN_SUCCESS, user: user[0],token: await tokenService.create({username:user.username,type_user:user.type_user}, { }) });
            }
            throw "error in login"
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.Verifed_OTP = async (req, res) => {
        try {
            const validate = await validatorService.schemas.verifyotpSchema.validate(req.body)
            if (validate.error) { throw validate.error.details[0].message }
            validate.value.modified = moment().format('YYYY-MM-DD h:mm:ss');
            const decordToken = await tokenService.decodedToken(validate.value.token)
            if(validate.value.phoneNo && validate.value.type_user === "customer" && validate.value.device_type ==="phone"){
                const exist = await db.select('*').from('users').where({ phoneNo: validate.value.phoneNo,type_user:validate.value.type_user });
                if (!exist[0]) { throw constant.USER_NOT_EXIST }
                if (exist[0].otp != validate.value.otp) {throw constant.USER_WRONG_OTP;}
                await db('users').where({ phoneNo: validate.value.phoneNo }).update({ otp: "", modified: validate.value.modified });
                return res.status(200).json({ success: true, message: constant.USER_VERIFY, data: exist, token: await tokenService.create({ phoneNo: validate.value.phoneNo,type_user:validate.value.type_user }, { }) });
            }
            throw "error in verify otp"
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.ForgetPassword = async (req, res) => {
        try {
            const validate = await validatorService.schemas.forgetpasswordSchema.validate(req.body)
            if (validate.error) { throw validate.error.details[0].message }
            if (validate.value.type === "user") {
                if (!await dbService.find(UserModel, { where: { phoneNumber: validate.value.phoneNumber } })) { throw constant.USER_NOT_EXIST }
                const token = await tokenService.create({ phoneNumber: validate.value.phoneNumber }, { expiresIn: '1h' })
                await dbService.update(UserModel, { token: token }, { where: { phoneNumber: validate.value.phoneNumber } });
                let sms = await smsService.OTPsend(token, validate.value, constant.Backend_URL) // 60 * 60 is one hour
                return res.status(200).json({ success: true, message: `Forget password mail send successfully.`, token: token });
            }
            // if (!await mailService.send(token,req.body.email)) { throw "Error in forget mail" }
        } catch (err) {
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.UpdatePassword = async (req, res) => {
        try {
            const validate = await validatorService.schemas.updatepasswordSchema.validate(req.body)
            if (validate.error) { throw validate.error.details[0].message }
            if (validate.value.type === "user") {
                const user = await dbService.find(UserModel, { where: { token: validate.value.token } })
                if (!user) { throw "token expire" }
                validate.value.password = await bcrypt.hash(validate.value.password, 10)
                const update = await dbService.update(UserModel, { password: validate.value.password, token: "" }, { where: { token: validate.value.token } });
                return res.status(200).json({ success: true, message: `Password update successfully.`, data: update, token: await tokenService.create({ phoneNumber: user.dataValues.phoneNumber, id: user.dataValues.id }, {}) });
            }
        } catch (err) {
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.UpdateProfile = async (req, res) => {
        try {
            const validate = await validatorService.schemas.updateProfileSchema.validate(req.body)
            if (validate.error) { throw validate.error.details[0].message }
            if (validate.value.type_user === "customer") {
                const update = await dbService.update(UserModel, validate.value, { where: { phoneNo: req.user.dataValues.phoneNo } });
                return res.status(200).json({ success: true, message: `Password update successfully.`, data: update });
            }
            // return res.status(200).json({ success: true });
        } catch (err) {
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.getUsers = async (req, res) => {
        try {
            const users = await db.raw(`SELECT COUNT(*) AS NumberOfUsers FROM users where type_user = 'customer'`);
            return res.status(200).json({ success: true, message: constant.SIGNUP_SUCCESSFULLY, data: users});
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
}

