// ------------------ Packages -------------------------
const moment = require('moment');
// ------------------ Services -------------------------
const ValidatorService = require('../services/validator.services');
const validatorService = new ValidatorService();
// ------------------ Model -------------------------
const constant = require('../db/constant');
// ------------------ dataBase -------------------------
const db = require('../db/config')

module.exports = StaticPageController = function () {
    this.addStaticPage = async (req, res) => {
        try {
            const validate = await validatorService.schemas.staticPage.validate(req.body)
            if (validate.error) { throw validate.error.details[0].message }
            const staticPage = await db('static_pages').insert(validate.value)
            return res.status(200).json({ success: true, message: constant.STATICPAGE_ADD, data: staticPage });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.getAllStaticPage = async (req, res) => {
        try {
            const staticPage = await db.select('*').from('static_pages')
            return res.status(200).json({ success: true, message: constant.STATICPAGE_GET_ALL, data: staticPage });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.getStaticPage = async (req, res) => {
        try {
            const staticPage = await db.select('*').from('static_pages').where({ id: req.params.id });
            return res.status(200).json({ success: true, message: constant.STATICPAGE_GET, data: staticPage[0] });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.updateStaticPage = async (req, res) => {
        try {
            const validate = await validatorService.schemas.staticPage.validate(req.body)
            if (validate.error) { throw validate.error.details[0].message }
            const staticPage = await db('static_pages').where({ id: req.params.id }).update(validate.value);
            return res.status(200).json({ success: true, message: constant.STATICPAGE_EDIT, data: staticPage });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.deleteStaticPage = async (req, res) => {
        try {
            const staticPage = await db('static_pages').where({ id: req.params.id }).del();
            return res.status(200).json({ success: true, message: constant.STATICPAGE_DELETE, data: staticPage });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
}