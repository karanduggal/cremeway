// ------------------ Packages -------------------------
const moment = require('moment');
// ------------------ Services -------------------------
const TokenService = require('../services/token.services');
const tokenService = new TokenService();
const ValidatorService = require('../services/validator.services');
const validatorService = new ValidatorService();
// ------------------ Model -------------------------
const constant = require('../db/constant');
// ------------------ dataBase -------------------------
const db = require('../db/config')

module.exports = RegionsController = function () {
    this.addRegions = async (req, res) => {
        try {
            const validate = await validatorService.schemas.regions.validate(req.body)
            if (validate.error) { throw validate.error.details[0].message }
            validate.value.created = moment().format('YYYY-MM-DD h:mm:ss');
            validate.value.modified = moment().format('YYYY-MM-DD h:mm:ss');
            const regions = await db('regions').insert(validate.value)
            return res.status(200).json({ success: true, message: constant.REGIONS_ADD, data: regions });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.getAllRegions = async (req, res) => {
        try {
            req.query.search = req.query ? req.query.search ? req.query.search : '' : '';
            req.query.pageNo = req.query ? req.query.pageNo ? req.query.pageNo : 0 : 0;
            // req.query.limit = req.query ? req.query.limit ? req.query.limit : 10 : 10;
            const count = await db.raw(`select count(*) as count FROM regions`)
            if(req.query.limit){
                req.query.limit = req.query ? req.query.limit ? req.query.limit : 10 : 10;
                if (req.query.pageNo != 0) { req.query.pageNo = (req.query.pageNo-1) *req.query.limit}  
                const regions = await db.raw(`SELECT * FROM regions WHERE name LIKE '%${req.query.search}%' limit ${req.query.pageNo},${req.query.limit};`)
                return res.status(200).json({ success: true, message: constant.AREAS_GET_ALL, data: regions[0],totalCount:count[0][0].count  });
            }   
            const regions = await db.select('*').from('regions')     
            return res.status(200).json({ success: true, message: constant.REGIONS_GET_ALL, data: regions,totalCount:count[0][0].count });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.getRegion = async (req, res) => {
        try {
            const regions = await db.select('*').from('regions').where({ id: req.params.id });
            return res.status(200).json({ success: true, message: constant.REGIONS_GET, data: regions[0] });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.updateRegion = async (req, res) => {
        try {
            const validate = await validatorService.schemas.regions.validate(req.body)
            if (validate.error) { throw validate.error.details[0].message }
            validate.value.modified = moment().format('YYYY-MM-DD h:mm:ss');
            delete validate.value.deviceType;
            const regions = await db('regions').where({ id: req.params.id }).update(validate.value);
            return res.status(200).json({ success: true, message: constant.REGIONS_EDIT, data: regions });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.deleteRegion = async (req, res) => {
        try {
            const regions = await db('regions').where({ id: req.params.id }).del();
            return res.status(200).json({ success: true, message: constant.REGIONS_DELETE, data: regions });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
}