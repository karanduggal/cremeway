// ------------------ Packages -------------------------
// const bcrypt = require('bcrypt');
// const jwt = require('jsonwebtoken');
const moment = require('moment');
const md5 = require('md5')
// ------------------ Services -------------------------
const TokenService = require('../services/token.services');
const tokenService = new TokenService();
// const MailService = require('../services/mail.services');
// const mailService = new MailService();
// const DBService = require('../services/DB.services');
// const dbService = new DBService();
const ValidatorService = require('../services/validator.services');
const validatorService = new ValidatorService();
// const SmsService = require('../services/sms.services');
// const smsService = new SmsService();
// ------------------ Model -------------------------
// const {UserModel} = require('../models')
// const UserModel = require('../models').users;
const constant = require('../db/constant');
// ------------------ dataBase -------------------------
const db = require('../db/config')

module.exports = ProductController = function () {
    this.addCategory = async (req, res) => {
        try {
            const validate = await validatorService.schemas.category.validate(req.body)
            if (validate.error) { throw validate.error.details[0].message }
            validate.value.created = moment().format('YYYY-MM-DD h:mm:ss');
            validate.value.modified = moment().format('YYYY-MM-DD h:mm:ss');
            delete validate.value.deviceType
            const category = await db('categories').insert(validate.value)
            return res.status(200).json({ success: true, message: constant.CATEGORY_ADD, data: category });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.getAllCategory = async (req, res) => {
        try {
            const category = await db.select('*').from('categories')
            return res.status(200).json({ success: true, message: constant.CATEGORY_GET_ALL, data: category });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.getCategory = async (req, res) => {
        try {
            const category = await db.select('*').from('categories').where({ id: req.params.id });
            return res.status(200).json({ success: true, message: constant.CATEGORY_GET, data: category[0] });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.updateCategory = async (req, res) => {
        try {
            const validate = await validatorService.schemas.category.validate(req.body)
            if (validate.error) { throw validate.error.details[0].message }
            validate.value.modified = moment().format('YYYY-MM-DD h:mm:ss');
            delete validate.value.deviceType;
            const update = await db('categories').where({ id: req.params.id }).update(validate.value);
            return res.status(200).json({ success: true, message: constant.CATEGORY_EDIT, data: update });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.deleteCategory = async (req, res) => {
        try {
            const Delete = await db('categories').where({ id: req.params.id }).del();
            return res.status(200).json({ success: true, message: constant.CATEGORY_DELETE, data: Delete });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.getAllProduct = async (req, res) => {
        try {
            req.query.category = req.query ? req.query.category ? req.query.category : '' : '';
            req.query.search = req.query ? req.query.search ? req.query.search : '' : '';
            req.query.pageNo = req.query ? req.query.pageNo ? req.query.pageNo : 0 : 0;
            req.query.limit = req.query ? req.query.limit ? req.query.limit : 10 : 10;
            if (req.query.pageNo != 0) { req.query.pageNo = (req.query.pageNo-1) *req.query.limit}            
            const count = await db.raw(`select count(*) as TotalNumberOfProducts FROM products`)
            fisrtPart = `SELECT 
            products.id,products.category_id, products.name, products.is_subscribable, products.price_per_unit, products.quantity, products.in_stock, products.iscontainer, products.unit_id, products.status, products.created, products.modified, products.image, products.description, products.position,
            categories.name as category_name, categories.category_image as category_image, categories.status as category_status , categories.is_deleteable as category_is_deleteable,
            units.name as unit_name, units.status as unit_status
            FROM products 
            LEFT JOIN categories ON products.category_id=categories.id
            LEFT JOIN units ON products.unit_id=units.id 
            WHERE products.name LIKE '%${req.query.search}%'`;
            secondPart = ` AND categories.name = '${req.query.category}'`;
            thirdPart = `limit ${req.query.pageNo},${req.query.limit};`
            if(req.query.fromHome){
                req.query.category = "Milk"
                secondPart = ` AND categories.name = '${req.query.category}'`;
                const milk = await db.raw(fisrtPart+secondPart+thirdPart);
                req.query.category = "Farm Fresh Products"
                secondPart = ` AND categories.name = '${req.query.category}'`;
                const Farm_Fresh_Products = await db.raw(fisrtPart+secondPart+thirdPart);
                req.query.category = "Milk Products"
                secondPart = ` AND categories.name = '${req.query.category}'`;
                const milkProduct = await db.raw(fisrtPart+secondPart+thirdPart);
                return res.status(200).json({ success: true, message: constant.PRODUCT_GET_ALL, milk:milk[0],Farm_Fresh_Products:Farm_Fresh_Products[0],milkProduct:milkProduct[0] });
            }
            if(req.query.category !== ""){
                const countByCategoty = await db.raw(`select 
                count(categories.name) as count ,categories.name
                FROM products
                LEFT JOIN categories ON products.category_id=categories.id
                group by categories.name;`)
                console.log('countByCategoty', countByCategoty)
                const product = await db.raw(fisrtPart+secondPart+thirdPart);
                return res.status(200).json({ success: true, message: constant.PRODUCT_GET_ALL, data: product[0],countByCategory:countByCategoty[0], totalCount: count[0][0].TotalNumberOfProducts });
            }
            const product = await db.raw(fisrtPart+thirdPart);
            // const product = await db.select('*').from('products')
            return res.status(200).json({ success: true, message: constant.PRODUCT_GET_ALL, data: product[0], totalCount: count[0][0].TotalNumberOfProducts });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.getProduct = async (req, res) => {
        try {
            const product = await db.raw(`SELECT 
                products.id,products.category_id, products.name, products.is_subscribable, products.price_per_unit, products.quantity, products.in_stock, products.iscontainer, products.unit_id, products.status, products.created, products.modified, products.image, products.description, products.position,
                categories.name as category_name, categories.category_image as category_image, categories.status as category_status , categories.is_deleteable as category_is_deleteable,
                units.name as unit_name, units.status as unit_status
                FROM products 
                LEFT JOIN categories ON products.category_id=categories.id
                LEFT JOIN units ON products.unit_id=units.id
                where products.id = ${req.params.id};`);
            const areas =[]
            var product_child
            // var product_areas
            if(req.query.deviceType=='cms'){
                // product_areas = await db.select('area_id').from('product_areas').where({ product_id: req.params.id })
                let product_areas = await db.select('*').from('product_areas').where({ product_id: req.params.id })
                for (let obj of product_areas) {
                    areas.push(obj.area_id)
                }
                product_child = await db.select('*').from('product_children').where({ product_id: req.params.id })
            }
            // const product = await db.select('*').from('products').where({ id: req.params.id });
            return res.status(200).json({ success: true, message: constant.PRODUCT_GET, data: product[0][0],areas: areas,product_child:product_child});
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.addProduct = async (req, res) => {
        try {
            if(typeof req.body.product === "string"){ req.body.product = JSON.parse(req.body.product);}
            if(req.body.product_child && typeof req.body.product_child === "string"){ req.body.product_child = JSON.parse(req.body.product_child);}
            if(req.body.areas &&typeof req.body.areas === "string"){ req.body.areas = JSON.parse(req.body.areas);}
            const validate = await validatorService.schemas.product.validate(req.body)
            if (validate.error) { throw validate.error.details[0].message }
            validate.value.product.created = moment().format('YYYY-MM-DD h:mm:ss');
            validate.value.product.modified = moment().format('YYYY-MM-DD h:mm:ss');

            let categortID_exist = await db('categories').where({ id: validate.value.product.category_id });
            if (!categortID_exist[0]) { throw constant.CATEGORY_NOT_EXIST }

            let unitsID_exist = await db('units').where({ id: validate.value.product.unit_id });
            if (!unitsID_exist[0]) { throw constant.UNIT_NOT_EXIST }

            if(validate.value.product_child){
                for (let child of validate.value.product_child) {
                    let unitsID_exist = await db('units').where({ id: child.unit_id });
                    if (!unitsID_exist[0]) { throw constant.UNIT_NOT_EXIST }
                }
            }
            if(validate.value.areas){
                for (let area of validate.value.areas) {
                    let areaId_exist = await db('areas').where({ id: area });
                    if (!areaId_exist[0]) { throw constant.AREAS_NOT_EXIST }
                }
            }

            const product = await db('products').insert(validate.value.product);
            const product_child =[]
            const areas =[]

            if(validate.value.product_child){
                if(!product[0]){ throw constant.PRODUCT_NOT_ADD}
                for (let child of validate.value.product_child) {
                    child.product_id = product[0]
                    const arr_child = await db('product_children').insert(child);
                    product_child.push(arr_child[0])
                }
            }
            if(validate.value.areas){
                if(!product[0]){ throw constant.PRODUCT_NOT_ADD}
                for (let areaId of validate.value.areas) {
                    console.log('areaId', areaId)
                    let area = {
                        product_id:product[0],
                        area_id:areaId
                    }
                    const arr_child = await db('product_areas').insert(area);
                    areas.push(arr_child[0])
                }
            }

            return res.status(200).json({ success: true, message: constant.PRODUCT_ADD, product: product,product_child:product_child,areas:areas });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.updateProduct = async (req, res) => {
        try {
            if(typeof req.body.product === "string"){ req.body.product = JSON.parse(req.body.product);}
            if(req.body.product_child && typeof req.body.product_child === "string"){ req.body.product_child = JSON.parse(req.body.product_child);}
            if(req.body.areas &&typeof req.body.areas === "string"){ req.body.areas = JSON.parse(req.body.areas);}
            const validate = await validatorService.schemas.product.validate(req.body)
            if (validate.error) { throw validate.error.details[0].message }
            validate.value.modified = moment().format('YYYY-MM-DD h:mm:ss');

            let categortID_exist = await db('categories').where({ id: validate.value.product.category_id });
            if (!categortID_exist[0]) { throw constant.CATEGORY_NOT_EXIST }

            let unitsID_exist = await db('units').where({ id: validate.value.product.unit_id });
            if (!unitsID_exist[0]) { throw constant.UNIT_NOT_EXIST }

            if(validate.value.product_child){
                for (let child of validate.value.product_child) {
                    let unitsID_exist = await db('units').where({ id: child.unit_id });
                    if (!unitsID_exist[0]) { throw constant.UNIT_NOT_EXIST }
                }
            }
            const deleteproductChild =[]
            const AllChild = await db.select('id').from('product_children').where({ product_id: req.params.id })
            for (let child of AllChild) {
                let found = false;
                for (const body_chi of validate.value.product_child) {
                    if(child.id ===  body_chi.id){
                        found = true
                        break;
                    }
                }
                if(!found){
                    deleteproductChild.push(child.id);
                }
            }
            if(validate.value.areas){
                for (let area of validate.value.areas) {
                    let areaId_exist = await db('areas').where({ id: area });
                    if (!areaId_exist[0]) { throw constant.AREAS_NOT_EXIST }
                }
            }

            const product = await db('products').where({ id: req.params.id }).update(validate.value.product);
            const product_child =[]
            const areas =[]

            if(validate.value.product_child){
                if(!product){ throw constant.PRODUCT_NOT_EDIT}

                for (let child of validate.value.product_child) {
                    if(child.id){
                        let updateChild = await db('product_children').where({ id: child.id}).update(child);
                        product_child.push(updateChild[0])
                    }else{
                        child.product_id =req.params.id
                        let updateChild = await db('product_children').insert(child);
                        product_child.push(updateChild[0])
                    }
                }
            }
            if(deleteproductChild.length > 0){
                for (let id of deleteproductChild) {
                    let deleteProChi = await db('product_children').where({ id: id }).del();
                }
            }
            return res.status(200).json({ success: true, message: constant.PRODUCT_EDIT, data: product });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
    this.deleteProduct = async (req, res) => {
        try {
            const Delete = await db('products').where({ id: req.params.id }).del();
            return res.status(200).json({ success: true, message: constant.PRODUCT_DELETE, data: Delete });
        } catch (err) {
            console.log('err', err)
            return res.status(200).json({ success: false, message: err });
        }
    }
}


// {
//     "product":{
//         "category_id":90,
//         "name":"zvfrhfr",
//         "is_subscribable":1,
//         "price_per_unit":50,
//         "quantity":100,
//         "in_stock":0,
//         "iscontainer":0,
//         "unit_id":26,
//         "status":1,
//         "image":"img/images/1560338902.png",
//         "description":"Organic Basmati Rice",
//         "position":9
//     },
//     "product_child":[
//         {
//             "quantity":90,
//             "in_stock":100,
//             "unit_id":26,
//             "price":50
//         },
//         {
//             "quantity":90,
//             "in_stock":100,
//             "unit_id":26,
//             "price":50
//         },
//         {
//             "quantity":90,
//             "in_stock":100,
//             "unit_id":26,
//             "price":50
//         }
//     ]
        // "areas":[
        //     12,34,21
        // ]
// }
